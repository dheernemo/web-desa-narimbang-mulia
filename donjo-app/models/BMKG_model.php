<?php 

/**
 */
class BMKG_model
{
	static public function getPrakiraan($location = 'Lebak')
	{
		setlocale(LC_ALL, 'IND');
		// $location = 'Lebak';
		$doc = new DomDocument();
		$doc->load('http://data.bmkg.go.id/datamkg/MEWS/DigitalForecast/DigitalForecast-Banten.xml');

		// intialize an DomXPath object
		$xpath = new DomXPath($doc);

		$data = [];
		$time = [
			'0000' => 'Pagi Hari',
			'0600' => 'Siang Hari',
			'1200' => 'Malam Hari',
			'1800' => 'Dini Hari',
		];

		$kodeCuaca = [
			0 => 'Cerah',
			100 => 'Cerah',
			1 => 'Cerah Berawan',
			101 => 'Cerah Berawan',
			2 => 'Cerah Berawan',
			102 => 'Cerah Berawan',
			3 => 'Berawan',
			103 => 'Berawan',
			4 => 'Berawan Tebal',
			104 => 'Berawan Tebal',
			5 =>'Udara Kabur',
			10 =>'Asap',
			45 =>'Kabut',
			60 =>'Hujan Ringan',
			61 =>'Hujan Ringan',
			63 =>'Hujan Lebat',
			80 =>'Hujan Lokal',
			95 =>'Hujan Petir',
			97 => 'Hujan Petir'
		];

		foreach ($xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[1]/timerange') as $timerange) {

			$datetime = $timerange->getAttribute('datetime');
			$date = substr($datetime, 0, 8);
			$temp = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="t"]/timerange[@datetime="'.$datetime.'"]/value')->item(0)->nodeValue;
			$tmax = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="tmax"]/timerange[@day="'.$date.'"]/value')->item(0)->nodeValue;
			$tmin = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="tmin"]/timerange[@day="'.$date.'"]/value')->item(0)->nodeValue;
			$humid = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="hu"]/timerange[@datetime="'.$datetime.'"]/value')->item(0)->nodeValue;
			$weather = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="weather"]/timerange[@datetime="'.$datetime.'"]/value')->item(0)->nodeValue;
			$wd = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="wd"]/timerange[@datetime="'.$datetime.'"]/value')->item(1)->nodeValue;
			$wd_val = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="wd"]/timerange[@datetime="'.$datetime.'"]/value')->item(0)->nodeValue;
			$ws = $xpath->query('/data/forecast/area[@description="'.$location.'"]/parameter[@id="ws"]/timerange[@datetime="'.$datetime.'"]/value')->item(2)->nodeValue;
			$ampm = in_array($time, ['1200', '1800']) ? 'pm' : 'am';

			if (\DateTime::createFromFormat('!YmdHi', $datetime)->format('Y-m-d H:i') > date('Y-m-d H:i', strtotime("-6 hour"))) {
				$data[] = [
					'date' => $date,
					'datetime' => $datetime,
					'tanggal' => strftime('%d %B %Y', strtotime(\DateTime::createFromFormat('!Ymd', $date)->format('Y-m-d'))),
					'temp' => $temp,
					'tmax' => $tmax,
					'tmin' => $tmin,
					'humid' => $humid,
					'weather' => $kodeCuaca[$weather],
					'wd' => $wd,
					'wd_val' => round($wd_val),
					'ws' => round($ws),
					'time' => $time[substr($datetime, 8, 4)],
					'image' => 'https://www.bmkg.go.id/asset/img/weather_icon/ID/' . strtolower($kodeCuaca[$weather]) . '-' . $ampm . '.png'
				];
			}
			
		}

		return $data;
	}
}