$(document).ready(function(){
	$("#widget-cuaca").owlCarousel({
		loop:true,
		items: 1,
		// nav:true,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true
	});

	$("#widget-cuaca-home").owlCarousel({
		loop:true,
		responsive:{
	        0:{
	            items:1,
	        },
	        1000:{
	            items:4,
	        }
	    },
		// nav:true,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true
	});
});